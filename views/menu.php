<!DOCTYPE html>
<html lang="es">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<title>Menu Ilerna</title>

		<link rel="stylesheet" href="assets/css/bootstrap.min.css">

		<script src="assets/js/bootstrap.min.js" ></script>

	</head>
	
	<body>
		<div class="container">

            <h1>Menu de Pizza Ilerna</h1>
            <h2>FCT Bloque 03 Ejercicio 1</h2>

            <a href="index.php?c=menu&a=nuevo" class="btn btn-primary">Crear nuevo Plato</a>

            <hr>

			<br />
			<div class="table-responsive">
				<table border="1" width="80%" class="table">
					<thead>
						<tr class="table-primary">
							<th>Plato</th>
							<th>Tipo</th>
							<th>precio</th>
							<th>valoracion</th>
							<th>Editar</th>
							<th>Eliminar</th>
						</tr>
					</thead>
					
					<tbody>
						<?php foreach($data["menu"] as $dato) {
							echo "<tr>";
							echo "<td>".$dato["plato"]."</td>";
							echo "<td>".$dato["tipo"]."</td>";
							echo "<td>".$dato["precio"]."</td>";
							echo "<td>".$dato["valoracion"]."</td>";
							echo "<td><a href='index.php?c=menu&a=modificar&id=".$dato["id"]."' class='btn btn-warning'>Modificar</a></td>";
							echo "<td><a href='index.php?c=menu&a=eliminar&id=".$dato["id"]."' class='btn btn-danger'>Eliminar</a></td>";
							echo "</tr>";
						}
						?>
					</tbody>
					
				</table>
			</div>
		</div>
	</body>
</html>