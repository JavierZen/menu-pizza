<!DOCTYPE html>
<html lang="es">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<title>Menu Ilerna</title>
		<link rel="stylesheet" href="assets/css/bootstrap.min.css">
		<script src="assets/js/bootstrap.min.js" ></script>
	</head>
	
	<body>
		<div class="container">

            <h1>Menu de Pizza Ilerna</h1>
            <h2>FCT Bloque 03 Ejercicio 1</h2>

            <hr>
            <br><br>


            <form id="nuevo" name="nuevo" method="POST" action="index.php?c=menu&a=guarda" autocomplete="off">

				<div class="form-group">
					<label for="plato">Plato</label>
					<input type="text" class="form-control" id="plato" name="plato" />
				</div>
				
				<div class="form-group">
					<label for="tipo">Tipo</label>

                    <select name="tipo" id="tipo" multiple>
                        <option value="Especialidades de la casa">Especialidades de la casa</option>
                        <option value="Las clásicas">Las clásicas</option>
                        <option value="Las más picantes">Las más picantes</option>
                        <option value="Para los más gourmets">Para los más gourmets</option>
                        <option value="Familiares">Familiares</option>
                        <option value="Aperitivos">Aperitivos</option>
                    </select>

				</div>
				
				<div class="form-group">
					<label for="precio">Precio</label>
					<input type="text" class="form-control" id="precio" name="precio" />
				</div>
				
				<div class="form-group">
					<label for="valoracion">Valoracion</label>
					<input type="text" class="form-control" id="valoracion" name="valoracion" />
				</div>
				
				<button id="guardar" name="guardar" type="submit" class="btn btn-primary">Guardar</button>
				
			</form>
		</div>
	</body>
</html>