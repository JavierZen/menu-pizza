<?php
	
	class menuController {
		
		public function __construct(){
			require_once "models/MenuModel.php";
		}
		
		public function index(){

			$menu = new Menu_model();
			$data["titulo"] = "Menu";
			$data["menu"] = $menu->get_menu();
			
			require_once "views/menu.php";
		}
		
		public function nuevo(){
			
			$data["titulo"] = "Menu";
			require_once "views/menu_nuevo.php";
		}
		
		public function guarda(){
			
			$plato = $_POST['plato'];
			$tipo = $_POST['tipo'];
			$precio = $_POST['precio'];
			$valoracion = $_POST['valoracion'];
			
			$menu = new Menu_model();
			$menu->insertar($plato, $tipo, $precio, $valoracion);
			$data["titulo"] = "Menu";
			$this->index();
		}
		
		public function modificar($id){
			
			$menu = new Menu_model();
			
			$data["id"] = $id;
			$data["menu"] = $menu->get_menus($id);
			$data["titulo"] = "Menu";
			require_once "views/menu_modifica.php";
		}
		
		public function actualizar(){

			$id = $_POST['id'];
			$plato = $_POST['plato'];
			$tipo = $_POST['tipo'];
			$precio = $_POST['precio'];
			$valoracion = $_POST['valoracion'];

			$menu = new Menu_model();
			$menu->modificar($id, $plato, $tipo, $precio, $valoracion);
			$data["titulo"] = "Menu";
			$this->index();
		}
		
		public function eliminar($id){
			
			$menu = new Menu_model();
			$menu->eliminar($id);
			$data["titulo"] = "Menu";
			$this->index();
		}	
	}
?>